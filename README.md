## Elgamal / baby step gaint step algorithm
    Finding the optimal generator for ElGamal cipher from a large prime p belong to G set.
    use this generator as an input for bsgs algorithm to find x

    h=g^x mod p

## Wiki
    https://en.wikipedia.org/wiki/ElGamal_encryption
    https://en.wikipedia.org/wiki/Cyclic_group
    https://en.wikipedia.org/wiki/Baby-step_giant-step
