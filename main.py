import random
import math


def isPrime(n, k):

    # Corner cases
    if n == 1 or n == 4:
        return False
    elif n == 2 or n == 3:
        return True

    # Try k times
    else:
        for i in range(k):

            # Pick a random number
            # in [2..n-2]
            # Above corner cases make
            # sure that n > 4
            a = random.randint(2, n - 2)

            # Fermat's little theorem
            if pow(a, n - 1, n) != 1:
                return False
    return True

# find a random large prime in range 2 - n


def random_high_prime(n):
    g = [i for i in range(2, n) if isPrime(i, 3)]
    prime = random.choice(g[int(len(g)/2)+1:])

    return prime, g

# clean the generated list from (duplicated, zero values)
# and then sort depending on sequence_list length


def cleanfy(generators):
    delete_dub = {x: list(dict.fromkeys(y)) for x, y in generators.items()}
    filtered_generators = {x: y for x, y in delete_dub.items() if y != 0}
    dic_generators = dict(
        sorted(filtered_generators.items(), key=lambda x: len(x[1])))

    return dic_generators

# Generate all possible sequence in range of 1-p


def sequence_generator(p):
    seq_generators = {}
    for i in range(1, p):
        seq = [pow(i, j, p) for j in range(0, p)]
        seq_generators[i] = seq
    seq_generators = cleanfy(seq_generators)
    return seq_generators


def bsgs(g, h, p):
    '''
    Solve for x in h = g^x mod p given a prime p.
   
    '''

    N = math.ceil(math.sqrt(p - 1))  # phi(p) is p-1 if p is prime

    # Create baby table
    tbl = {pow(g, i, p): i for i in range(N)}

    # Precompute via Fermat's Little Theorem
    c = pow(g, N * (p - 2), p)

    # Search for an equivalence in the table. Giant step.
    for j in range(N):
        y = (h * pow(c, j, p)) % p
        if y in tbl:
            return j * N + tbl[y]

    # Solution not found
    return None


def do_find_generator(init_p):

    p, primes = random_high_prime(init_p)

    print("************ Finding a CG ************ ", end="\n")
    print("primes (1-{}): {}".format(init_p, primes))
    print("picked random large p: {}".format(p))

    seq_generators = sequence_generator(p)

    for k, v in seq_generators.items():
        print("{}: {}".format(k, v), end="\n")
    last_key = list(seq_generators)[-1]
    last_value = seq_generators[last_key]

    print("************ result************")
    print("Optimal generator g = {}\nElements included from initial p - 1 = {} elements: {} \ng^i= {}^i: {}".format(
        last_key, p, len(last_value), last_key, last_value), end="\n")

    return last_key, last_value


def main():

    while True:
        init_p = int(
            input("Enter the range of the initial p (prime number): "))
        if isPrime(init_p, 4):
            break

    while True:
        to_find = int(input("Enter a number h to find x in range inital p:"))
        if to_find < init_p:
            break
        else:
            print("Enter a value less than inital p")

    g, _ = do_find_generator(init_p)
    bg = bsgs(g, to_find, init_p)

    print("************ BSGS Algorithm ************")
    if bg is None:
        print("**** try again to get the X ****")
    else:
        print("h = {}, g = {}, init_p = {}".format(to_find, g, init_p))
        print("x= {}\n h = g^x mod p => {}= {}^{} mod {}".format(
            bg, to_find, g, bg, init_p))


if __name__ == "__main__":
    main()
